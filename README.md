# GZdoom

Doom is a source port for the modern era, supporting current hardware and operating systems and sporting a vast array of user options. Make Doom your own again!
Maximum Game Support
In addition to Doom, ZDoom supports Heretic, Hexen, Strife, Chex Quest, and fan-created games like Harmony and Hacx. Meet the entire idTech 1 family!

Under GPL 3 license.

Website: https://zdoom.org/

Source Code: https://github.com/coelckers/gzdoom

AppImages: https://gitlab.com/ose-appimages/gzdoom/-/releases

List of compatible games: https://zdoom.org/wiki/IWAD

## How to run:

* Download the AppImage and give it execution permissions.
* Get a **legal copy** of the game on Doom, Doom II, etc, or get [Freedoom](https://freedoom.github.io/) absoluty free. and maybe, some mods.
* Copy the the game and mods .wad files in the same AppImage folder.
* Double ckick in the AppImage, select game and enjoy ;)
